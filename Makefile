BIN=demo

CC=g++
#CC=clang++

#CPPFILES=$(shell ls *.cpp)
HFILES=$(shell ls *.h)
DEPFILES=$(HFILES) Makefile

DEBUG  =-O0 -D_DEBUG -g
RELEASE=-O3 -DNDEBUG

INCLUDES=
WARNS=-Wall -Wextra -Wold-style-cast -Weffc++ -pedantic#-Wno-variadic-macros
OPTS=-std=c++14 $(WARNS) $(INCLUDES) $(DEBUG)
LINKOPT=-rdynamic# -rdynamic for demo backtrace dump

%.o: %.cpp $(DEPFILES)
	@ echo $(CC) $<
	@ $(CC) $(OPTS) -c $< -o $@ 

$(BIN): main.o demo.o prepostcondition.o
	@ echo $(CC) $^
	@ $(CC) $(OPTS) $(LINKOPT) $^ -o $@ 

.PHONY:valgrinder
valgrinder: $(BIN)
	valgrind --leak-check=full --show-leak-kinds=all -v $(BIN)	

lokitest: Etc/IncludeEx/loki/test/Checker/main.cpp Makefile
	$(CC) $(OPTS) -IEtc/IncludeEx/loki/include  -Wno-unused-parameter -Wno-pedantic $< -o $@ 

contracttest: Etc/IncludeEx/contractpp-code/example/stroustrup97/string_main.cpp Etc/IncludeEx/contractpp-code/example/stroustrup97/string.cpp
	$(CC) $(DEBUG) -std=c++98 -IEtc/IncludeEx/boost -IEtc/IncludeEx/contractpp-code/include $^ -o $@ 

smartasserttest: Etc/IncludeEx/smart_assert/libs/smart_assert/example/assert_example.cpp Makefile
	$(CC) $(DEBUG) -std=c++98 -IEtc/IncludeEx/smart_assert -IEtc/IncludeEx/boost  $< -o $@  -Wno-effc++

.PHONY:testex
testex: lokitest contracttest #smartasserttest

Q=-q
.PHONY: test	
test: $(BIN)
	@ $(BIN) $(Q) && echo "TEST: OK"

.PHONY: alltests
alltests: $(BIN) test
	@ $(BIN) $(Q) -1
	@ $(BIN) $(Q) -2
	@ $(BIN) $(Q) -3
	@ $(BIN) $(Q) -4
	@ $(BIN) $(Q) -5
	@ $(BIN) $(Q) -6

.PHONY: setup
setup:
	sudo apt install g++ # needed 
	sudo apt install clang indent c++filt # optional
	sudo apt install kdbg valgrind # for code inspections, my favorites
	
.PHONY: clean
clean:
	@ rm -f $(BIN) *.o out.txt lokitest contracttest smart-asserttest

