#include "prepostcondition.h"
#include "helpers.h"
#include "tester.h"

int user_fun_demos(const int n);

namespace tests
{
	using std::string;
	using std::endl;
	using std::exception;
	
	using namespace helpers;
	using namespace helpers::debug;
	using namespace prepostcondition;
		
	// global vars
   tester g_t;
	
	#ifdef TEST
	  "ERROR: TEST already defined"
	#endif
  	#define TEST(b) g_t.test((b),DBG_SRC.tostring())
   
   class test_output
   {	
   	private:
   	 	static int _level;
   	 	
		public:			
			static void setlevel(const int level) 
			{
				_level = level;
			}
			
			static bool getlevel()
			{
				return _level;
			}
			
			static bool getquiet()
			{
				return _level<0;
			}
			
			void flush()
			{
				if (!getquiet()) std::cout.flush();
			}	
					
			template<typename T>
			test_output& operator<<(const T& t)
			{
				if (!getquiet()) std::cout << tostring(t);
				return *this;
			}
			
			test_output& operator<<(std::ostream& (*)(std::ostream&)) 
			{
				if (!getquiet()) std::cout << "\n";
         	return *this;
			}
		
			static void print(const std::string& s)
			{	
				if (!getquiet())
				{
					std::cout.flush();
					std::cerr << s << std::endl;
				}
			 }
   };

	int test_output::_level;
   test_output tout;

	typedef condition<error_handler<test_output> > test_default_condition;

#ifdef _DEBUG
   void TestDbgClasses()
   {
   	const nulldbg nd;
		const dbg d0("debug",1,"dummy1","dummy2");
		const int l=__LINE__;
		const dbg d1(__FILE__,__LINE__,__FUNCTION__,"msg");
		const dbg d2=DBG_SRC;
		const dbg d3(nullptr,-1,nullptr,nullptr);
		TEST(nd.tostring()=="");
		TEST(d0.tostring()!="");
		TEST(nd.empty());
		TEST(!d0.empty());
		const string s0=d1.tostring();
		//TEST(s0.size()>20 && s0.substr(0,4)=="main" && s0.substr(s0.size()-4,s0.size())=="msg");
		const string s1=d1.tostring();
		TEST(s1.size()>20 && s1.substr(0,4)==string(__FILE__).substr(0,4) && d2.msg()==nullptr);
		TEST(d1.line()==l+1);
		TEST(d2.line()==l+2);
		TEST(string(d1.file())==__FILE__);
		TEST(d3.empty());
		TEST(d3.file()==nullptr);
		TEST(d3.line()==-1);
		TEST(d3.fun()==nullptr);
		TEST(d3.msg()==nullptr);
	}
   
	void TestErrorClasses()
	{		
		// try instantiate some templates types
		const error_handler<>   eh("error handler");
		const error_exception<> ex("error exception");
		TEST(eh.what() == string("error handler"));
		TEST(ex.what() == string("error exception"));
	}
	
	void TestConditions()
	{
		const nulldbg nd;
		const dbg d("debug",1,"dummy1","dummy2");
		
		// try instantiate some template types
		condition<error_handler<test_output> > c0;		
		condition<error_exception<test_output> > c1;
		condition<error_exception<test_output>,nulldbg> c2(nd);
		condition<error_exception<test_output>,dbg> c3(d);
		condition<error_exception<test_output>,dbg> c4(DBG_SRC);
		
		TEST(c0(true));
		TEST(!c0(false));

		int x=0;
		try {TEST(!c0(false));} 
		catch (const prepostcondition::exception& ex) {++x;}
		TEST( x == 0);
		
		TEST(c1(true));
		try {TEST(!c1(false));} 
		catch (const prepostcondition::exception& ex) {++x;}
		TEST( x == 1);
		
		// NOTE: catch specific template ex does not work yet...
   	//x=0;
		//try {TEST(!c1(false));} 
		//catch (const condition<error_exception<> >& ex) {++x;} // NOTE: should us a 'typeof(c1)' or something
		//TEST( x == 1);

		TEST(c2(true));
		x=0;
		try {TEST(!c2(false));} 
		catch (const prepostcondition::exception& ex) {++x;}
		TEST( x == 1);

		TEST(c3(true));		
		x=0;
		try {TEST(!c3(false));} 
		catch (const prepostcondition::exception& ex) {++x;}
		TEST( x == 1);

		TEST(c4(true));		
		x=0;
		try {TEST(!c4(false));} 
		catch (const prepostcondition::exception& ex) {++x;}
		TEST( x == 1);
	}	
	
	int TestLambdaexpression_sub(int x,int y)
	{
		return x+y;
	}
	
	void TestLambdaexpression()
	{
		{
		 	auto n0 = []() {return true;};
         lambda_expression<decltype(n0)> l0(n0);

			TEST(n0());
			TEST(l0());

			int m=1;
			auto n1 = [&]() {return m==1;};
			lambda_expression<decltype(n1)> l1(n1);
			TEST(n1());
			m=42;
			TEST(!l1());
		}
		{		
		
		/*				
			int n=2,first=n;
			auto f0 = [&]() {return first==n;};
			auto f1 = [&]() {return first==n+11;};
		
			auto l0=make_lambda_expression<>(f0);
			auto l1=make_lambda_expression<>(f1);
			lambda_expression_base* b0=&l0;
			lambda_expression_base* b1=&l1;
			
			TEST(b0!=b1);
			TEST( f0());
			TEST( !f1());
				
			TEST(l0());
			TEST(l0.eval());
			TEST(b0->eval());
			TEST(!l1());
			TEST(!l1.eval());
			TEST(!b1->eval());
			
			first += 11;
			TEST(!l0());
			TEST(!l0.eval());
			TEST(!b0->eval());
			TEST(l1());
			TEST(l1.eval());
			TEST(b1->eval());

			//auto n2 = [&]() {return m>1;};
			//lambda_expression<> l1(n2);
			
			//auto l2=make_lambda_expression<>(n1);
			//lambda_expression<decltype(n1),bool> l3=make_lambda_expression<>(n1,bool());
			//auto l4=make_lambda_expression<>(n1);
			*/
		}
		
		class ftest
		{
			public:
				bool operator()() const
				{
					return true;
				}
				
				int operator()(int x) const
				{
					return x+1;
				}
		};
		
		// pointer to class member funs
		ftest ft;
	   lambda_expression<decltype(ft),bool> lt0(ft);
	   lambda_expression<decltype(ft),int,int> lt1(ft);
	   TEST(lt0());
	   TEST(lt1(1)==2);	   
	
	   // old style pointer to fun
	   int(*ft2)(int,int)=TestLambdaexpression_sub;
	   TEST(ft2(1,2)==3);
	   //lambda_expression<decltype(ft2),int,int,int> lt2(ft2); // not possible yet
	   //TEST(lt2(2,3)==5);
	}
	
	void TestPostconditions()
	{
		int n=0;
		auto f = [&]() {return n==10;};
		postcondition<decltype(f), test_default_condition> p0(f);
		TEST(!f());
		TEST(!p0());
		n=10;
		TEST(f());
		TEST(p0());
		TEST(p0);

		n=1;		
		postcondition<decltype(f),test_default_condition> p1(f);
		TEST(!f());
		TEST(!p1());
		
		n=10;
		TEST(f());
		TEST(p1());
		TEST(p1);
	}

	int runtestsuite()
	{
		tout << "testsuite...";
		TestDbgClasses();
		TestErrorClasses();
		TestConditions();
		TestLambdaexpression();
		TestPostconditions();
		return g_t.tests_failed()==0 ? 0 : -1;
	}
#else
	int runtestsuite()
	{
		return 0;
	}
#endif

	int fun1(int& n) 
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		int first = n;
		
		auto f0 = [&]() {return first==n+1;};
		postcondition<lambda_expression<decltype(f0)>, test_default_condition > p0(f0,DBG_SRC);		
		p0.autodisable();
		TEST(!p0());		
		first += 1;
		TEST(p0());

		first = n;
		auto f1 = [=]() {return first==n+1;};
		postcondition<lambda_expression<decltype(f1)>, test_default_condition > p1(f1,DBG_SRC);
		p1.autodisable();
		TEST(!p1());		
		first += 1;
		TEST(!p1()); // condition still false due to [=] binding
		
		return 0;
	}

	int fun2(int& n) 
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		int first = n;
		
		auto f = [&]() {return first==n;};
		postcondition<lambda_expression<decltype(f)>, test_default_condition > p(f,DBG_SRC);
		
		TEST( p() );
		first += 10;
		TEST( !p() );
		
		return 0;
	}

	int fun3(int& n) 
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		int first = n;
		
		auto f = [&]() {return first==n;};
		postcondition<lambda_expression<decltype(f)>, test_default_condition > p(f,DBG_SRC);
		
		TEST( p() );
		first += 10;
		TEST( !p() );
		
		return 0;
	}

	int fun4(int& n) 
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		int first = n;
		
		auto f = [&]() {return first==n+10;};
		postcondition<lambda_expression<decltype(f)>, condition<error_exception<test_output>,dbg> > p(f,DBG_SRC);

		int x=0;	
		try{p();}
		catch (const prepostcondition::exception& ex) {++x;}
		TEST(x==1);

		first += 10;
		x=0;
		try{p();}
		catch (const prepostcondition::exception& ex) {++x;}
		TEST(x==0);

		return 0;
	}

#define POSTCOND(n,a) auto n = [&]() {return a;};\
	prepostcondition::postcondition<prepostcondition::lambda_expression<decltype(n), bool>, test_default_condition > postcond_##n(n,DBG_SRC)

#define POST_RET(r,c) if (!(c)) {error_handler<>::err(DBG_SRC.tostring() + "POST_RET failed");}\
	return r;
   
	int fun5(int& n)
	{	
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		int first = n;
		
		POSTCOND(p0,n-1 == first );
		POSTCOND(p1,3 == n );
		TEST( !postcond_p0() );
		TEST( !postcond_p1() );
		{
			int l=n+1;
			POSTCOND(p2,l == first );
			TEST( !postcond_p2() );
			l -= 1;
			TEST( postcond_p2() );
		}

		++n;
		TEST(postcond_p0());
		TEST(postcond_p1());
		
		POST_RET(n, n > first);		
	}

	int fun6(int& n) 
	{	
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		/*	
		int first=n;
		auto f0 = [&]() {return first==n;};
		auto f1 = [&]() {return first==n+11;};
	
		first += 11;
	
		postconditions<error_handler<test_output> > p0;
		first = n;		
		p0.push_back( f0, DBG_SRC);
		p0.push_back( [&]() {return first==n;}, dbg(__FILE__,__LINE__,__FUNCTION__,"p0 lambda 0"));
		first += 10;
		p0.push_back( [=]() {return first==n+10;}, dbg(__FILE__,__LINE__,__FUNCTION__,"p0 lambda 1"));

		TEST(p0.errors()==0);
		TEST(!p0.ret());
		TEST(p0.errors()==2);
		first = n;		
		TEST(p0.ret());
		TEST(p0.errors()==0);

		first = n + 13;
		postconditions<error_exception<test_output> > p1;
		p1.push_back( f1, DBG_SRC );
		p1.push_back( [&]() {return first==n+11;}, dbg(__FILE__,__LINE__,__FUNCTION__,"p1 lambda 0"));
		p1.push_back( [=]() {return first==n+13;}, dbg(__FILE__,__LINE__,__FUNCTION__,"p1 lambda 1"));
		p1.push_back( [&]() {return first==n+11;}, DBG_SRC);
	
		TEST(!f1());
		
		int x=0;
		TEST(p1.errors()==0);
		try {TEST(!p1.ret());}
		catch (const prepostcondition_exception& ex) {++x;}
		TEST(x==1);
		TEST(p1.errors()==1); // two errors, but due to throw one error reported

		first = n+11;		
		TEST(p1.ret());
		TEST(p1.errors()==0);
			
		return p0.ret_val(0) + p1.ret_val(0);
		*/
		return 0;
	}

	int fun7(int& n) 
	{	
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		/*
		int first = n+12;
		auto f0=[&]() {return first==n+11;};
		auto f1=[=]() {return first==n+12;};
		
		TEST(!f0());
		TEST(f1());
		
		postconditions<error_exception<test_output> > p;
		p.push_back( [&]() {return first==n+11;}, dbg(__FILE__,__LINE__,__FUNCTION__,"p lambda 0"));
		p.push_back( [=]() {return first==n+12;}, dbg(__FILE__,__LINE__,__FUNCTION__,"p lambda 1"));
		TEST(p.errors()==0);

		int x=0;	
		try{p.ret();}
		catch (const prepostcondition::exception& ex) {++x;}
		TEST(x==1);
		TEST(p.errors()==1);
		
		first=n+11;	
		TEST(f0());
		TEST(f1());

		x=0;
		try{p.ret();}
		catch (const prepostcondition::exception& ex) {++x;}
		TEST(x==0);
		TEST(p.errors()==0);
					
		return p.ret_val(0);
		*/
		return 0;
	}

	int fun8(int& n,const bool recurse=true)
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		(void)recurse;
		/*
		if (recurse) 
		{
			int m=0;
			const int r1=fun8(--m,false);
			const int r2=fun8(--m,false);
			const int r3=fun8(--m,false);
			const int r4=fun8(--m,false);
			TEST(r1==-1);
			TEST(r2==-2);
			TEST(r3==-3);
			TEST(r4==1);
		}
			
		int r=n;
		auto fr = [&]() {return r<0;};
		
		postconditions<error_handler<test_output> > p0;
		postconditions<error_exception<test_output> > p1;
		
		if (r==-1) return p0.ret_val(r,fr,DBG_SRC);
		else if (r==-2) return p0.ret_val(r,[&](){return r==-2;},DBG_SRC);	
		else if (r==-3) return p1.ret_val(r,[&](){return r==-3;},DBG_SRC);	
		else if (r==-4) {
			TEST(p0.errors()==0);
			int rr=p0.ret_val(p0.errors(),[&](){return r!=-4;},DBG_SRC);
			TEST(rr==0);
			TEST(p0.errors()==1);
			return p0.ret_val(p0.errors(),[&](){return r!=-4;},DBG_SRC);	
		}
		else return 0;
		*/
		return 0;
	}

	void fun9(int& n)
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;		
		/*
		postconditions<error_handler<test_output> > p0;
		postconditions<error_exception<test_output> > p1;
		
		p0.ret_void();
		p1.ret_void();
		p0.ret_void();
		p1.ret_void();
		*/	
		return;
	}
	
	/*
	int fun10_sub(int& n,postconditions<error_handler<test_output> >& p)
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
	
		TEST( n==2 );
		int first=n-1;
		auto f3 = [&]() { tout << "f3: first=" << first << ", n=" << n << ", &first=" << &first << ", &n=" << &n << endl; return first<n;};
		p.push_back(f3,DBG_SRC);

		TEST( p.ret());
		TEST( p.errors()==0);

		first = n+1;
		TEST( !p.ret());

		first = n-1;
		TEST( p.ret());
		
		return p.ret_val(0);
	}
	*/
	
	int fun10(int& n)
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		/*
		postconditions<error_handler<test_output> > p;
		auto f0 = [&]() {tout << "f0: n=" << n << ", &n=" << &n << endl; return n==2;};
		p.push_back(f0,DBG_SRC);
		TEST(n==2);

		trace(1) << "before block.." << endl;
		{
			int first=n;
			auto f1 = [&]() {tout << "f1: first=" << first << ", n=" << n << ", &first=" << &first << ", &n=" << &n << endl; return first==n+1;};
			p.push_back(f1,DBG_SRC);
			TEST( p.size()==2);
			TEST( p.errors()==0);
			TEST(!p.ret());
			TEST( p.errors()==1);
			first = n+1;
			TEST( p.ret());
			TEST( p.errors()==0);
		}
		trace(1) << "after block.." << endl;
		
		TEST( p.size()==2);
		TEST( p.ret());
		n=3;
		TEST( !p.ret());
		n=2;
		TEST( p.ret());
	
		fun10_sub(n,p);
		TEST( p.size()==3);
		TEST( n==2 );
		TEST( p.ret());
		
		return 0;
		n=0;
		TEST( !p.ret());
		TEST( p.errors()==3);
		n=1;
		TEST( !p.ret());
		TEST( p.errors()==3);
				
		return p.ret_val(0);
		*/
		return 0;
	}		

typedef condition<error_handler<test_output> > my_condition;
#define MY_CONDITION my_condition

	int fun11(int& n)
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		/*
		int first=n;
		
		PRE( first==n );
		PRE( n==2 );
		PRE( n!=2 );
		
		POST_PREP;

		p.push_back([](){return true;});
		POST( first==n+1 );
		POST( first+1==n+2 );
		POST( first+2==n+3 );
	
		TEST( !p.ret() );
		TEST( p.errors()==3);
		TEST( p.size()==4);
		first = n+1;
		
		RETURN_POST(0,first==n+1);
		*/
		return 0;
	}

//typedef condition<error_exception<test_output> > my_condition2;
//#define MY_CONDITION my_condition2

	int fun12(int& n)
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		{
			int first=n;
				
			PRE( first==n );
			PRE( n==2 );
			PRE( n!=2, "pre, should fail" );
			
			auto f=[&](){return first==n+1;}; 
			auto p0=makers::make_postcondition<lambda_expression<decltype(f),bool>,test_default_condition>(f,dbg(__FILE__,__LINE__,__FUNCTION__,"p1, should fail once"));
			auto p1=makers::make_postcondition<lambda_expression<decltype(f),bool>,test_default_condition>(f,dbg(__FILE__,__LINE__,__FUNCTION__,"p2, copied, two conditions fails"));
			auto p2=prepostcondition::helpers::post(MY_CONDITION(),__FILE__,__LINE__,__FUNCTION__,makers::make_lambda_expression(f),"message");
			
			POST( first+1==n+2);
			POST( first+1==n+2, "post3");
			POST( false, "post, should fail");
			TEST(!f());
			TEST(!p0());
			TEST(!p1());
			TEST(!p2());

			{
				auto p3=p2;
				TEST(!p3());
			}		
			
			first = n + 1;
			TEST(f());
			TEST(p0());
			TEST(p1());
			TEST(p2());
			
			{
				auto p3=p2;
				TEST(p3());
			}		
		}
	   
	   int first = n;
		POST( first==n+1 );
		POST( first+1==n+2 );
		POST( first+2==n+3 );
		{
			int m=n+3;
			POST( m==n+4);
			++m;
		}
		{
			int m=n+3;
			POST( m==n+4, "should fail");
		}
		
		first = n+1;
		if (first==0) RETURN_POST(0,first==n+1);
		else RETURN_POST(0,first==n+1,"last ret");
	}

	int fun13(int& n)
	{
		trace(0) << __FUNCTION__ << "(" << n << ").." << endl;
		int first=n;
				
		PRE( first==n );
		PRE( n==2 );
		PRE( n!=2 );

		auto f=[&](){return first==n+1;}; 
		auto p=makers::make_postcondition<lambda_expression<decltype(f),bool>,test_default_condition>(f,dbg(__FILE__,__LINE__,__FUNCTION__,"sdf"));
		
		first=n+1;
		//p("p0,msg0");
		TEST(p());
		//p("p0,msg1");
		TEST(p());
		
		return 0;   
	}

	int printexception(const exception* const ex,const char*const what,const char*const msg,const dbg& d)
	{
		tout.flush();
		test_output::print("EXCEPTION: " + d.tostring() + "caught " + msg + (nullptr!=ex ? string("\twhat:") + what : ""));
		return -1;
	}

	int usage(const string& argv0)
	{
		std::cerr << "USAGE: " << argv0 << " [-q | -v | -w]  [-f] [-a | -u | -t | -1 .. -N] ";
		#ifdef _DEBUG 
			std::cerr << "(_DEBUG)";
		#endif
		#ifdef NDEBUG 
			std::cerr << "(NDEBUG)";
		#endif
		std::cerr << endl;
		std::cerr << "\t-q: quiet, default=false" << endl;
		std::cerr << "\t-v: verbose" << endl;
		std::cerr << "\t-w: extra verbose" << endl;
		std::cerr << "\t-f: exit when test fails, default=false" << endl;
		std::cerr << "\t-a: run all tests" << endl;
		std::cerr << "\t-u: run user demo only" << endl;
		std::cerr << "\t-t: run test suite only" << endl;
		std::cerr << "\t-1 .. -N: run test i only" << endl;
		return -2;
	}

	int submain(int argc, char **argv,bool recurse=false) 
	{
		bool q=false;
		try {
			const string argv0=argv[0];
			int m=0,v=0;
			bool f=false;
			
			for(int i=1;i<argc;++i)
			{
				const string a=argv[i];
				
				for(int j=1;j<64;++j)
				{
					const string s="-" + tostring(j);
					if (a==s) {
						if (m!=0) return usage(argv0);
						m=j;
						break;
					}
				}
				
				if (a=="-a") m=0;
				else if (a=="-t") m=-1;
				else if (a=="-q") q=!q;
				else if (a=="-f") f=!f;
				else if (a=="-u") m=-2;
				else if (a=="-v") v += 1;
				else if (a=="-w") v += 10;
				else if (a=="-?") {usage(argv0); return 0;}
				else if (m<=0) return usage(argv0);
			}
			
			g_t.setfail(f);
			tout.setlevel(q ? -1 : 0);
			trace.setlevel(q ? -1 : v);
			
			int n=2;		
					
#ifdef _DEBUG
			if (m==-1 || m==0) TEST( runtestsuite()==0);
			if (m==1 || m==0) TEST(fun1(n)==0 && n==2);
			if (m==2 || m==0) TEST(fun2(n)==0 && n==2);
			if (m==3 || m==0) TEST(fun3(n)==0);
			if (m==4 || m==0) TEST(fun4(n)==0);
			if (m==5 || m==0) TEST(fun5(n)==3 && n==3);
			if (m==6 || m==0) TEST(fun6(n)==0);
			if (m==7 || m==0) TEST(fun7(n)==0);
			if (m==8 || m==0) TEST(fun8(n)==0);
			if (m==9 || m==0) fun9(n);
#endif
			//if (m==10 || m==0) TEST(fun10(n)==0);
			if (m==11 || m==0) TEST(fun11(n)==0);
			if (m==12 || m==0) TEST(fun12(n)==0);
			if (m==13 || m==0) TEST(fun13(n)==0);
			if (m==-2 || m==0) TEST(user_fun_demos(2)==0);
			if (m<-2) throw std::runtime_error("missing fun call for mode=" + tostring(m));
			
			std::cout << "OK: " << g_t << endl;
			
			return g_t.tests_failed();
		} 
		catch (const prepostcondition::exception& ex)
		{
			printexception(&ex,ex.what(),"prepostcondition_exception",DBG_SRC);
		}
		catch (const std::exception& ex)
		{
			printexception(&ex,ex.what(),"exception",DBG_SRC);
		}
		catch (...)
		{
			printexception(nullptr,nullptr,"unknown exception",DBG_SRC);
		}
		
		if (!q && !recurse)
		{
			assert(argc+1<256);
			char* argv2[256];
			for(int i=0;i<argc;++i) {
				argv2[i]=argv[i];
			}
			char q[] = {'-', 'q', '\0'}; // or whatever str cp
			argv2[argc]=q;
			std::cout << "WARNING: "<< DBG_SRC.tostring() <<  "errors encountered, relaunching in non-quiet mode..." << std::endl;
			return submain(argc+1,argv2,true);
		}
		
		return -1;
	}
	
	#undef TEST
} // end tests

int main(int argc, char** argv) 
{
	return tests::submain(argc,argv);
}
