#include "prepostcondition.h"
#include "tester.h"

#define DEMO_BACKTRACE

// configure user prepostconditions
#define MY_CONDITION prepostcondition::condition<>

int user_fun_demo_simple(const int n)
{
	int first=n;
			
	PRE( first==n );
	PRE( n==2 );
	PRE( n!=2, "pre, should fail" ); 

	POST( first==n+1 );
	POST( first==3, "p2");

	{
		POST( first-1==n,"post, should fail");
		POST( false,"post trivial, should fail" );
	}

	first=n+1;
	if (first==0) RETURN_POST(0,first==n+2);
	else RETURN_POST(0,first==n-1,"ret, should fail");
}

class userclass
{
	private:
		int _x;
		
		bool invariant() const
		{
			return _x>2;
		}
		
		int fun(int m) const 
		{
			return 39+m;
		}
		
	public:
		userclass() : _x(3) 
		{
			POST( invariant() );
		}
		
		void fun(const int n)
		{
			PRE( invariant() );
			_x=0;
			POST( invariant() );
			// do some stuff
			_x=n;
		}	
		
		int operator()() const 
		{
			POST( invariant() );
			POST( _x>3 );
			POST( this->fun(3)==42 );
			return _x;
		}

		void mustfail() const
		{
			PRE( _x > 0);
			POST( _x < -42,"must fail");
		}
};

int user_fun_demo_classinvariants(const int n)
{
	(void)n;
	userclass x;
	x.fun(4);
	POST(  x()>3 );
	
	x.fun(5); // should not hold, when n<=2
	x.mustfail();
	
	return 0;
}

// re-configure user prepostconditions
#undef MY_CONDITION
#define MY_CONDITION prepostcondition::condition<prepostcondition::error_exception<> >

int user_fun_demo_exceptions(const int n)
{
	int c=0;
	try{
		PRE(false,"false hardcoded (0)");
	} 
	catch (const prepostcondition::exception& x)	{
		++c;
	}
	try {
		if (n==3) {
			POST(false,"false hardcoded (1)"); // will cause terminate
		}
	} 
	catch (const prepostcondition::exception& x)	{
		++c;
	}
	
	return n!=3 && c==1 ? 0 : -1;
}

#ifdef DEMO_BACKTRACE
#include <execinfo.h>
#include <unistd.h>
template<class OUTPUTER = prepostcondition::error_output>
class error_handler_backtrace : public prepostcondition::error_handler<OUTPUTER>
{
	private:
		typedef prepostcondition::error_handler<OUTPUTER> base;

      static std::string cppfilter(const std::string& s)
      {
      	FILE* f=nullptr;
      	try {
				const size_t n=s.find_first_of('('),m=s.find_first_of(')');
				if (std::string::npos==n || std::string::npos==m || n>=m) return s;
			
				const std::string t=s.substr(n+1,m-n-1);
				if (t=="") return s;
				assert( std::string::npos == t.find('"') );
				assert( std::string::npos == t.find('\'') );
			
				std::ostringstream cmd;
				cmd << "echo '" << t << "' | c++filt 2>/dev/null"; // NOTE: you must apt-get install c++filt
				//std::cout << "org='" + s;
				//std::cout << "cmd='" << cmd.str() << "'" << std::endl;
				
				f = popen(cmd.str().c_str(), "r");
				if (f!=nullptr) {
					std::string r;
					char buffer[1024];
					int n;
					while ((n = fread(buffer, 1, sizeof buffer, f)) > 0) {
						for(int j=0;j<n;++j) {
							const char c=buffer[j];
							if (c!='\n') r += c;
						}
					}
					const int e=pclose(f);
					if (e!=0) return s;
					return r + s.substr(m+1);
				}
				else return s;
			} 
			catch (...)
			{
				if (f!=nullptr) pclose(f);
				throw;
			}
		}	
			
		static void print_backtrace()
		{
			char** bt_syms = 0;
			try
			{
				void *bt[1024];
				const int bt_size = backtrace(bt, 1024);
				bt_syms = backtrace_symbols(bt, bt_size); //NOTE: must compile wiht -rdynamic for debug symbols
				
				OUTPUTER::print("BACKTRACE:");
				for(int i=1; i<bt_size; ++i) {
					std::string s;
					int j=0;
					while(true) {
						const char c=bt_syms[i][j++];
						if (c=='\0') break;
						s += c; // NOTE: i dont care about scaling and performace here, perhaps you do?
					}
					// NOTE: perhaps a c++filt on the string...
					s = "\t" + cppfilter(s);
					if (s.size()>80) s = s.substr(0,110) + "...";
					OUTPUTER::print(s);
				}
				free(bt_syms);
			}
			catch (...) {
				if (bt_syms) free(bt_syms);
				throw;
			}
		}
			
	public:
		explicit error_handler_backtrace(const std::string& what_arg) : base(what_arg) {}
		explicit error_handler_backtrace(const char*const what_arg)   : base(what_arg) {}
		
		virtual ~error_handler_backtrace() {}
		
		static void err(const std::string& what_arg)
		{	
			base::err(what_arg);
			print_backtrace();	         	
		}
};

// re-configure user prepostconditions
#undef MY_CONDITION
#define MY_CONDITION prepostcondition::condition<error_handler_backtrace<> >

void myfunc2(void)
{
	PRE(false,"pre, gimme a backtrace");
	POST(false,"post, gimme a backtrace");
}

void myfunc(int ncalls)
{
	if (ncalls > 1) myfunc(ncalls - 1);
	else myfunc2();
}

int user_fun_demo_backtrace(const int n)
{
	error_handler_backtrace<>::err("show me a backtrace");	
	myfunc(n);
	return 0;
}
#else
int user_fun_demo_backtrace(const int n)
{
	return 0;
}
#endif

#undef PRE
#define PRE(...) (void)nullptr
#undef POST
#define POST(...) (void)nullptr
#undef RETURN_POST
#define RETURN_POST(...) return prepostcondition::helpers::return_post_ndebug(__VA_ARGS__)

int user_fun_demo_ndebug(const int n)
{
	int first=n;
			
	PRE( first==n );
	PRE( n==2 );
	PRE( n!=2, "pre, should fail" ); 

	POST( first==n+1 );
	POST( first==3, "p2");

	{
		POST( first-1==n,"post, should fail");
		POST( false,"post trivial, should fail" );
	}

	first=n+1;
	if (first==0) RETURN_POST(0,first==n+2);
	else RETURN_POST(0,first==n-1,"ret, should fail");
}

int user_fun_demos(const int n)
{
	std::cout << "user demos..." << std::endl;
	const int t0=prepostcondition::statistics::tests();
	const int o0=prepostcondition::statistics::oks();
	const int e0=prepostcondition::statistics::exceptions();

	int r=0;
	r += user_fun_demo_simple(n);
	r += user_fun_demo_classinvariants(n);
	r += user_fun_demo_exceptions(n);
	r += user_fun_demo_backtrace(n);
	r += user_fun_demo_ndebug(n);		
	if (r!=0) return -1;
	
	const int t1=prepostcondition::statistics::tests();
	const int o1=prepostcondition::statistics::oks();
	const int e1=prepostcondition::statistics::exceptions();
	std::cout << prepostcondition::statistics() << std::endl;
	
	if (t1-t0!=25) r=-2;
	else if (o1-o0!=17) r=-3;
	else if (e1-e0!=1) r=-4;
	
	if (r!=0) {
		std::cout << "diff: tests=" << t1-t0 << ", oks=" << o1-o0 << ", exceptions=" << e1-e0  << std::endl;
		std::cout << "some diffs found, r=" << r << std::endl;
	}
		
	return r;
}
