#include <cassert>
#include <iostream>

namespace tests
{
	class tester
	{
		private:
			int _N_tests,_N_ok,_exit_code;
			bool _halt_after_fail;
			
		public:
			tester(const bool halt_after_fail=false) : _N_tests(0), _N_ok(0), _exit_code(-1), _halt_after_fail(halt_after_fail) {}
	
			void setfail(const bool f)
			{
				_halt_after_fail = f;
			}
	
			int tests_ok() const {return _N_ok;}
			int tests_failed() const 
			{
				assert( _N_tests>=_N_ok);
				return _N_tests- _N_ok;
			}
				
			void test(const bool b,const std::string& msg)
			{
				++_N_tests;
				if (!b) {
					std::cout.flush();
					std::cerr << "TEST: failed" << (!msg.empty() ? std::string(", " + msg) : "") << std::endl;
					if (_halt_after_fail) {
						std::cerr << "  EXIT().." << std::endl;
						exit(_exit_code);
					}
				}
				else ++_N_ok;
			}
			
			friend std::ostream& operator<<(std::ostream& s,const tester& x)
			{
				return s << "tester: oks=" << x.tests_ok() << ", failed=" << x.tests_failed();
			}
	};	
} // end tests
