#include <cassert>
#include <sstream>
#include <iostream>

#ifdef HELPERS_HAS_PRETTY_FUNCTION
  "ERROR: HELPERS_HAS_PRETTY_FUNCTION already defined"
#endif
#ifdef TRACE_FUN
	"ERROR: TRACE_FUN already defined"
#endif 

#define HELPERS_HAS_PRETTY_FUNCTION

#ifdef _DEBUG
	#ifdef HELPERS_HAS_PRETTY_FUNCTION
		#define TRACE_FUN(i,n) helpers::debug::trace(i) << "TRACE:" << __PRETTY_FUNCTION__ << n << ".." << std::endl;	
		#undef HELPERS_HAS_PRETTY_FUNCTION
	#else
		#define TRACE_FUN(i,n) helpers::debug::trace(i) << "TRACE:" <<  __FUNCTION__ << n << ".." << std::endl;	
	#endif
#else
	#define TRACE_FUN(i,n)	
#endif

namespace helpers
{
	template<typename T>	
	std::string tostring(const T& t)
	{
		std::ostringstream s;
		s << t;
		return s.str();
	}
	
	namespace debug
	{			
		class nulldbg 
		{
			public:
				nulldbg() {} // NOTE: needed to keep clang++ happy
				
				bool empty() const {return true;}
				
				template<typename T>
				std::string operator+(const T&)
				{
					return ""; // helpers::debug::tostring(t);
				}		

				template<typename T>
				friend std::string operator+(const T&,const nulldbg&)
				{
					return ""; // helpers::debug::tostring(t);
				}						
				
				std::string tostring() const {return "";}
		};
		
		class nulltracer 
		{	
			public:
				nulltracer& operator()(const int)
				{
					return *this;
				} 

				int setlevel(const int) {return 0;}
	 
				nulltracer& operator<<(std::ostream& (*)(std::ostream&)) 
				{
					return *this;
				}

				template<typename T> nulltracer& operator<<(const T&) 
				{
					return *this;
				}
		};
#ifdef _DEBUG		
		/*
		void do_assert(const bool b,const char*const file,const int line,const char*const fun)
		{
			if (!b) {
				std::cerr << "ASSERT: failed...";
			}
		}
		#define assert(b) prepostcondition::debug::do_assert(b,__FILE__,__LINE__,__FUNCTION__)
		*/
				
		class tracer 
		{	
			private:
				std::ostream* _s;
				int _l;
				tracer* _nulltracer;
				
				tracer(const tracer&);
				void operator=(const tracer&);
				
			public:
				tracer(std::ostream* s,const int l) :  _s(s), _l(l), _nulltracer(nullptr) {}
				~tracer()
				{
					if (nullptr!=_nulltracer) delete _nulltracer;
				}
	 
				tracer& operator()(const int l)
				{
					if (l<=_l) return *this;
					else {
						if (_nulltracer==nullptr) _nulltracer=new tracer(nullptr,0);
						assert(nullptr!=_nulltracer);
						return *_nulltracer;
					}
				}
				
				int setlevel(const int l)
				{
					int old=_l;
					_l=l;
					return old;
				}
	 
				tracer& operator<<(std::ostream& (*)(std::ostream&)) 
				{
					return *this << "\n"; 
				}

				template<typename T> 
				tracer& operator<<(const T& t) 
				{
					if (nullptr!=_s) (*_s) << t;
					return *this;
				}
		};

		tracer trace(&std::cout,0);	
#else
		class dbg
		{
			public:
				typedef const char* type_char_pointer;

				dbg(type_char_pointer,const int,type_char_pointer,type_char_pointer) {}
				dbg() {}
				
				std::string tostring() const {return "";}
		};

		typedef dbg default_dbg;

		debug::nulltracer trace;
#endif		
	} // end namespace debug
} // end namespace helpers
