#include <cassert>
#include <sstream>
#include <iostream>

#ifdef PREPOSTCONDITION_HAS_PRETTY_FUNCTION
  "ERROR: PREPOSTCONDITION_HAS_PRETTY_FUNCTION already defined"
#endif
#ifdef DBG_SRC
	"ERROR: DBG_SRC already defined"
#endif

#define PREPOSTCONDITION_HAS_PRETTY_FUNCTION

#ifdef _DEBUG
	#ifdef PREPOSTCONDITION_HAS_PRETTY_FUNCTION
		#define DBG_SRC prepostcondition::dbg(__FILE__,__LINE__,__PRETTY_FUNCTION__,nullptr)
		#undef PREPOSTCONDITION_HAS_PRETTY_FUNCTION
	#else
		#define DBG_SRC prepostcondition::dbg(__FILE__,__LINE__,__FUNCTION__,nullptr)
	#endif
#else
	#define DBG_SRC prepostconditions::dbg()
#endif

namespace prepostcondition
{
	class dbg
	{
		public:
			typedef const char* type_char_pointer;
			
		private:
			type_char_pointer const _file,_fun,_msg,_expr;
			const int _line;
			
			bool isempty() const
			{
				return _line==-1;
			}
			
			bool invariant() const
			{
				if (_line==-1) return isempty() && _file==nullptr && _fun==nullptr && _expr==nullptr &&_msg==nullptr;
				else return !isempty() && _file!=nullptr && _fun!=nullptr && _line>0;
			}
			
		public:
			dbg(type_char_pointer const file,const int line,type_char_pointer const fun,type_char_pointer const msg,type_char_pointer const expr=nullptr) : _file(file), _fun(fun), _msg(msg), _expr(expr), _line(line) 
			{
				assert(invariant());
			}
	
			dbg(const dbg& x) : _file(x._file), _fun(x._fun), _msg(x._msg), _expr(x._expr), _line(x._line)
			{
				assert(invariant());
			}
			
			dbg() : _file(nullptr), _fun(nullptr), _msg(nullptr), _expr(nullptr), _line(-1) 
			{
				assert(invariant());
			}
			
			// NOTE: if members are const no = is possible anyway
			//dbg& operator=(const dbg& x)
			//{
			//	assert(x.invariant());
			//	_file=x._file;
			//	_fun=x._fun;
			//	_msg=x._msg;
			//	_expr=x._expr;
			//	_line=x._line;
			//	assert(invariant());
			//}			
							
			type_char_pointer file() const
			{
				assert(invariant());
				return _file;
			}

			int line() const
			{
				assert(invariant());
				return _line;
			}
			
			type_char_pointer fun() const
			{
				assert(invariant());
				return _fun;
			}

			type_char_pointer msg() const
			{
				assert(invariant());
				return _msg;
			}

			type_char_pointer expr() const
			{
				assert(invariant());
				return _expr;
			}
			
			bool empty() const
			{
				assert(invariant());
				return isempty();
			}
	
			std::string tostring() const
			{
				assert(invariant());
				if (isempty()) return "";
				std::ostringstream s;
				s << _file << ":" << _line << ": " << _fun << (_msg!=nullptr || _expr!=nullptr ? ": " : "") << (_msg==nullptr ? "" : _msg) << (_expr==nullptr ? "" : (_msg==nullptr ? "" : ", ")) << (_expr==nullptr ? "" : _expr);
				return s.str();
			}
		
			//std::string operator+(type_char_pointer const s) const
			//{
			//	return this->tostring() + s;
			//}	
			//					
			//friend std::string operator+(type_char_pointer const s,const dbg& x)
			//{
			//	return s + x.tostring();
			//}		
			//
			//friend std::string operator+(const std::string& s,const dbg& x)
			//{
			//	return s + x.tostring();
			//}
	
			friend std::ostream& operator<<(std::ostream& s,const dbg& x)
			{
				return s << x.tostring();
			}
	};

	struct error_output
	{
		static void print(const std::string& s)
		{
			std::cout.flush();
			std::cerr << s << std::endl;
		}
	};

	class statistics
	{
		private:
			static int _tests, _oks, _exceptions;  // NOTE: no thread safety, if needed use atomic add or a locking policy instead

		public:			
			virtual ~statistics() {}
			
			static void addtest(const bool b) 
			{
				++_tests;
				if (b) ++_oks;
			}
			
			static void addexception() 
			{
				++_exceptions;
			}
			
			static int tests()      {return _tests;}			
			static int oks()        {return _oks;}
			static int exceptions() {return _exceptions;}
			
			friend std::ostream& operator<<(std::ostream& s,const statistics& x)
			{
				// NOTE: really just calls to static funs
				return s << "statistics: test=" << x.tests() << ", oks=" << x.oks() << ", exceptions=" << x.exceptions();
			}	
	};

	class exception : public std::runtime_error
	{
		private:
			const std::string _what_arg; // NOTE: could possible be stored direct in runtime_error base class

		public:
			explicit exception(const std::string& what_arg) : std::runtime_error("N/A"), _what_arg(what_arg) {}
			explicit exception(const char*const what_arg)   : std::runtime_error("N/A"), _what_arg(what_arg) {}

			virtual ~exception() {}
		
			virtual const char* what() const throw()
			{
				return _what_arg.c_str();
			}
			
			friend std::ostream& operator<<(std::ostream& s,const exception& x)
			{
				return s << "exception: what_arg=" << x.what();
			}	
	};

	template<class OUTPUTER = error_output>
	class error_handler : public exception
	{
		public:
			explicit error_handler(const std::string& what_arg) : exception(what_arg) {}
			explicit error_handler(const char*const what_arg)   : exception(what_arg) {}
			
			virtual ~error_handler() {}
			
			static void err(const std::string& what_arg)
			{
				OUTPUTER::print("ERROR: " + what_arg);
			}
	
			static void warn(const std::string& what_arg)
			{
				OUTPUTER::print("WARN:  " + what_arg);
			}
			
			friend std::ostream& operator<<(std::ostream& s,const error_handler&)
			{
				return s << "error_handler<" << __PRETTY_FUNCTION__ << ">";
			}	
	};

	template<class OUTPUTER = error_output,class STATISTICS = statistics>
	class error_exception : public error_handler<OUTPUTER>
	{
		private:
			static bool is_stack_unwinding()
			{
				return std::uncaught_exception();
			}
	
		public:
			explicit error_exception(const std::string& what_arg) : error_handler<OUTPUTER>(what_arg) {}
			explicit error_exception(const char* const what_arg)  : error_handler<OUTPUTER>(what_arg) {}
			
			virtual ~error_exception() {}
						
			static void err(const std::string& what_arg)
			{
				error_handler<OUTPUTER>::err(what_arg);
				if (is_stack_unwinding()) {
					error_handler<OUTPUTER>::warn(DBG_SRC.tostring() + ", defer throwing exception (throwing inside stack unwinding will cause a terminate)");
				}
				else {
					STATISTICS::addexception();
					throw error_exception(what_arg); // NOTE: will cause terminate when thrown in dtor
				}
			}
	};
	
	template<class ERRORHANDLER = error_handler<>,class DBG = dbg,class STATISTICS = statistics>
	class condition
	{
		public:
			typedef DBG type_dbg;
			typedef condition<ERRORHANDLER,DBG> type;
		
		private:
			const DBG _dbg;
			
			//condition& operator=(const condition& c)
			//{
			//	_dbg=c._dbg;
			//	return *this;
			//}
			//void operator()(const char*const msg)	
			//{
			//	_dbg.msg(msg);		
			//}
			
			condition& operator=(const condition&);
			
		public:
			condition() : _dbg() {} // NOTE: : _dbg() only to keep compiler warn -Weffc awai
			condition(const type_dbg& dbg) : _dbg(dbg) {}
			condition(const condition& x) : _dbg(x._dbg) {}		
			
			bool operator()(const bool b) const
			{
				STATISTICS::addtest(b);
				if (!b)
				{
					ERRORHANDLER::err("condition failed" + (_dbg.empty() ? "" : " in " + _dbg.tostring()));
					return false;
				}
				return true;
			}
			
			friend std::ostream& operator<<(std::ostream& s,const condition& x)
			{
				return s << "condition(" << x._dbg.tostring() << ")";
			}
	};	
			
	template<typename OT,typename RT = bool,typename ... A>
	class lambda_expression
	{
		// FROM: http://stackoverflow.com/questions/28746744/passing-lambda-as-function-pointer
		// also see http://lists.boost.org/Archives/boost/2005/04/84332.php
		//
		// OT => Object Type
		//// RT => Return Type
		//// A ... => Arguments
		//template<typename OT, typename RT, typename ... A>
		//struct lambda_expression {
		//	 OT _object;
		//	 RT(OT::*_function)(A...)const;
		//	 lambda_expression(const OT & object)
		//		  : _object(object), _function(&decltype(_object)::operator()) {}
		//	 RT operator() (A ... args) const {
		//		  return (_object.*_function)(args...);
		//	 }
		//};
		
		private:
			OT _object;
			RT(OT::*_function)(A...) const;

			#ifdef _DEBUG			
			bool invariant() const
			{
				//if (nullptr == reinterpret_cast<const void*>(&_object)) return false;
				//if (nullptr == reinterpret_cast<const void* const>(_function)) return false;
				return true;
			}
			#endif

			//void swap(lambda_expression& l)
			//{
			//	OT o=_object;
			//	_object=l._object;
			//	l._object = o;		
			//	auto f=_function;
			//	_function=l._function;
			//	l._function = f;
			//}	
			//
			//lambda_expression& operator=(const lambda_expression l)
			//{
			//	swap(l);
			//	assert(invariant());
			//	return *this;
			//}

			lambda_expression& operator=(const lambda_expression);
			
		public:
			lambda_expression(const OT& object) : _object(object), _function(&OT::operator()) // _function(&decltype(_object)::operator())
			{
				assert(invariant());
			}
		
			lambda_expression(const lambda_expression& x) : _object(x._object), _function(x._function) 
			{
				assert(invariant());
			}
			
			RT operator() (A ... args) const
			{
				assert(invariant());
				return (_object.*_function)(args...);
			}
	};
   
	template<class LAMBDA,class CONDITION = condition<> >
	class postcondition 
	{
		public:
			typedef postcondition<LAMBDA,CONDITION> type;
	
		private:			
			const LAMBDA _lambda;
			const CONDITION _condition;
	
			mutable bool _autodisabled;
			
			//void swap(postcondition& x)
			//{
			//	const LAMBDA l=_lambda;
			//	_lambda=x._lambda;
			//	x._lambda=l;
			//	const CONDITION c=_condition;
			//	_condition=x._condition;
			//	x._condition=c;
			//	const bool a=_autodisabled;
			//	_autodisabled=x._autodisabled;
			//	x._autodisabled=a;
			//}
			//
			//postcondition& operator=(const postcondition x)
			//{
			//	swap(x);
			//	//x._autodisabled=true;
			//	return *this;
			//}
			
			postcondition& operator=(const postcondition&);
			
		public:
			postcondition(LAMBDA l,typename CONDITION::type_dbg dbg=typename CONDITION::type_dbg()) :  _lambda(l), _condition(dbg), _autodisabled(false)
			{
			}

			postcondition(const postcondition& x) : _lambda(x._lambda), _condition(x._condition), _autodisabled(x._autodisabled)
			{
				x._autodisabled=true;
			}
									
			~postcondition()
			{
				if (!_autodisabled) 
				{
					//NOTE: throwing inside a dtor will yield a full TERMINATE of the executable!
					//NOTE: if on condition fail and throws, subsequent contitions might also fail.
					this->operator()();
					_autodisabled = true;
				}
			}

			operator bool() const
			{
				return this->operator()();
			}
						
			bool operator()() const
			{	
				return _condition(_lambda());
			}
			
			void autodisable()
			{
				_autodisabled = false;
			}
	};

	namespace makers 
	{
		template<class OT,typename RT = bool> 
		lambda_expression<OT,RT> make_lambda_expression(const OT& object, const RT=RT())
		{
			return lambda_expression<OT,RT>(object);
		} 
		
		template<typename OT,class CONDITION = condition<>,class DBG = dbg> 
		postcondition<lambda_expression<OT,bool>,CONDITION> make_postcondition(const OT& object,DBG d=DBG())
		{
			const lambda_expression<OT,bool> l=make_lambda_expression<OT,bool>(object);
			const postcondition<lambda_expression<OT,bool>,CONDITION> p(l,d);
			return p;
		} 
	}
	
	namespace helpers
	{
		inline bool get_arg_0(bool b,const char*const msg=0)
		{
			(void)msg;
			return b;
		}

		inline const char* get_arg_1(bool b,const char*const msg=0)
		{
			(void)b;
			return msg;
		}

		template<class CONDITION = condition<> >
		bool pre(const CONDITION&,const char*const file,const int line,const char*const fun,const bool a,const char* const msg=0)
		{
			const typename CONDITION::type_dbg d(file,line,fun,msg);
			const CONDITION c(d);
			return c(a);
		}
	
		template<class OT,class CONDITION = condition<> >
		auto post(const CONDITION&,const char*const file,const int line,const char*const fun,prepostcondition::lambda_expression<OT,bool> f,const char* const msg=0)
		{
			const typename CONDITION::type_dbg d(file,line,fun,msg);
			auto p=makers::make_postcondition<lambda_expression<OT,bool>,CONDITION>(f,d);
			return p;
		}
		
		template<typename T,class CONDITION = condition<> >
		T return_post(const CONDITION& c,const char*const file,const int line,const char*const fun,T t,const bool a,const char* const msg=0)
		{
			pre(c,file,line,fun,a,msg);
			return t;
		}

		template<typename T>
		T return_post_ndebug(T t,const bool,const char* const msg=0)
		{
			(void)msg;
			return t;
		}

		#ifdef PRE
		  "ERROR: PRE already defined"
		#endif
		#ifdef POST
		  "ERROR: POST already defined"
		#endif
		#ifdef RETURN_POST
		  "ERROR: RETURN_POST already defined"
		#endif
		#ifdef PREPOSTCONDITION_TOKENPASTE
		  "ERROR: PREPOSTCONDITION_TOKENPASTE already defined"
		#endif
		#ifdef PREPOSTCONDITION_TOKENPASTE2
		  "ERROR: PREPOSTCONDITION_TOKENPASTE2 already defined"
		#endif

		#ifdef NDEBUG
			#define PRE(...) (void)nullptr
			#define POST(...) (void)nullptr
			#define RETURN_POST(...) return prepostcondition::helpers::return_post_ndebug(__VA_ARGS__)
		#else
			#define PREPOSTCONDITION_TOKENPASTE(x, y) x ## y
			#define PREPOSTCONDITION_TOKENPASTE2(x, y) PREPOSTCONDITION_TOKENPASTE(x, y)

			#define PRE(...) prepostcondition::helpers::pre(MY_CONDITION(),__FILE__,__LINE__,__PRETTY_FUNCTION__,__VA_ARGS__)
			#define POST(...) auto PREPOSTCONDITION_TOKENPASTE2(autovar_lambda_f_, __LINE__) = [&]() {return prepostcondition::helpers::get_arg_0(__VA_ARGS__);}; auto PREPOSTCONDITION_TOKENPASTE2(autovar_postcondition_, __LINE__)=prepostcondition::helpers::post(MY_CONDITION(),__FILE__,__LINE__,__PRETTY_FUNCTION__,prepostcondition::makers::make_lambda_expression(PREPOSTCONDITION_TOKENPASTE2(autovar_lambda_f_, __LINE__)),prepostcondition::helpers::get_arg_1(__VA_ARGS__))
			#define RETURN_POST(...) return prepostcondition::helpers::return_post(MY_CONDITION(),__FILE__,__LINE__,__PRETTY_FUNCTION__,__VA_ARGS__)
		#endif
	}
} // end prepostcondition namespace

